class Settings {
  static const String WORKOUT_MANAGER_BASE_URL = "https://wger.de/api/v2/";

  static const Map<ENDPOINTS, String> MGER_ENDPOINTS = {
    ENDPOINTS.daysofweek: "daysofweek",
    ENDPOINTS.exercise: "exercise",
    ENDPOINTS.exercisecategory: "exercisecategory",
    ENDPOINTS.exerciseimage: "exerciseimage",
    ENDPOINTS.language: "language",
    ENDPOINTS.muscle: "muscle",
    ENDPOINTS.equipment: "equipment",
    ENDPOINTS.exerciseinfo: "exerciseinfo"
  };

  static const String IMAGE_MAIN_BACKGROUND =
      "assets/images/main_screen_back.jpg";

  static const double IMAGE_MAIN_BACKGROUND_BLUR = 5.0;
}

enum ENDPOINTS {
  daysofweek,
  equipment,
  exercise,
  exerciseinfo,
  exercisecategory,
  exercisecomment,
  exerciseimage,
  ingredient,
  ingredienttoweightunit,
  language,
  license,
  muscle,
  weightunit,
  settingrepetitionunit,
  settingweightunit,
}
