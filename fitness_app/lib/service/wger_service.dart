import 'package:fitness_app/common/settings.dart';
import 'package:http/http.dart' as http;

class WorkoutManagerService {
  /// static instance of [WorkoutManagerService] to implement [Singleton] design.
  static WorkoutManagerService _instance =
      new WorkoutManagerService._internal();

  /// Factory constructor that always returns a static instance.
  factory WorkoutManagerService() => _instance;

  /// Private constructor.
  WorkoutManagerService._internal();

  /// Retrives `WorkoutManager's` api data from the specified [endPoint].
  /// The Rertrived data could be in any language according to the api
  /// available translations and `english` is default [language = 0].
  ///
  /// This method Retrives only the approved data from `WorkoutManager's`
  /// api by default [status = 2].
  ///
  /// You can Specify the filtering [options] by passing a map of options
  /// as an argument.
  ///
  /// You can also specify a map of [headers].
  ///
  /// Returns a Future of [http.Response] which contains the needed data
  /// about `WorkoutManager's` api response.
  Future<http.Response> get(ENDPOINTS endPoint,
      {int language = 0,
      int status = 2,
      Map<String, dynamic> options,
      Map<String, String> headers}) async {
    http.Response response;
    String optionsString =
        options == null ? "" : _generateOptionsString(options);
    String url =
        "${Settings.WORKOUT_MANAGER_BASE_URL}${Settings.MGER_ENDPOINTS[endPoint]}/$optionsString";
    response = await http.get(url, headers: headers);
    return response;
  }

  /// Converts [options] map into a string.
  String _generateOptionsString(Map<String, dynamic> options) {
    String optionsString = "?";
    options.forEach((option, value) {
      optionsString += "$option=${value.toString()}&";
    });
    return optionsString.substring(0, optionsString.length - 1);
  }
}
