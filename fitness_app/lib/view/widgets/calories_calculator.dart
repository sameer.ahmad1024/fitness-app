import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CaloriesCalculator extends StatefulWidget {
  @override
  CaloriesCalculatorState createState() => CaloriesCalculatorState();
}

class CaloriesCalculatorState extends State<CaloriesCalculator> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        color: Color.fromARGB(150, 150, 150, 175),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 10),
                alignment: Alignment.topCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("CALORIES CALCULATOR",
                        style: TextStyle(
                            fontFamily: "ABeeZee",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Text("Daily Plan ",
                              style: TextStyle(
                                  fontFamily: "ABeeZee",
                                  fontSize: 16,
                                  color: Colors.white)),
                          Text("130 CAL",
                              style: TextStyle(
                                  fontFamily: "Orbitron",
                                  fontSize: 16,
                                  color: Colors.white))
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text("Consumed",
                            style: TextStyle(
                                fontFamily: "ABeeZee",
                                fontSize: 16,
                                color: Colors.white)),
                        Text("130 CAL",
                            style: TextStyle(
                                fontFamily: "Orbitron",
                                fontSize: 16,
                                color: Colors.white))
                      ],
                    )
                  ],
                ),
              ),
              flex: 3,
            ),
            Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 3)),
              child: Center(
                child: Text(
                  "ADD",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Orbitron",
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ));
  }
}
