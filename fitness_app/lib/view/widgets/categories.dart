import 'package:fitness_app/view/widgets/svg_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Categories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Container(
          color: Colors.white,
          width: 145,
          margin: EdgeInsets.only(left: 20),
          height: 157,
          child: Center(
            child: SvgImage(
              color: Color.fromARGB(255, 15, 50, 74),
              asset: "assets/icons/trademill.svg",
              width: 70,
              height: 70,
            ),
          ),
        ),
        Container(
          color: Color.fromARGB(200, 90, 113, 138),
          width: 145,
          height: 157,
          margin: EdgeInsets.only(left: 20),
          child: Center(
            child: SvgImage(
              color: Colors.white,
              asset: "assets/icons/sports.svg",
              width: 70,
              height: 70,
            ),
          ),
        ),
        Container(
          color: Color.fromARGB(200, 90, 113, 138),
          width: 145,
          height: 157,
          margin: EdgeInsets.only(left: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Flexible(
                  flex: 2,
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 10,
                      height: 10,
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 5,
                  child: Center(
                    child: SvgImage(
                      color: Colors.white,
                      asset: "assets/icons/eggs.svg",
                      width: 70,
                      height: 70,
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Text(
                    "Diet Plans",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "ABeeZee",
                        fontSize: 24),
                  ),
                ),
              ]),
        ),
      ],
    );
  }
}
