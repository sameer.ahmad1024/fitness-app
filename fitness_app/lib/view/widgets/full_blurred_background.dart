import 'package:fitness_app/common/settings.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class FullBlurredBackground extends StatelessWidget {
  final String _image;
  final double _blur;
  FullBlurredBackground(
      {String image = Settings.IMAGE_MAIN_BACKGROUND,
      double blur = Settings.IMAGE_MAIN_BACKGROUND_BLUR})
      : _image = image,
        _blur = blur;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage(_image), fit: BoxFit.cover),
      ),
      child: BackdropFilter(
        filter: ui.ImageFilter.blur(
          sigmaX: _blur,
          sigmaY: _blur,
        ),
        child: Container(
          decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
        ),
      ),
    );
  }
}
