import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

class SvgImage extends StatelessWidget {
  final Color _color;
  final String _asset;
  final double _width;
  final double _height;
  SvgImage({Color color, String url, String asset, double width, double height})
      : _color = color,
        _asset = asset,
        _width = width,
        _height = height;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      _asset,
      height: _height,
      width: _width,
      color: _color,
      semanticsLabel: 'A red up arrow',
      fit: BoxFit.fill,
    );
  }
}
