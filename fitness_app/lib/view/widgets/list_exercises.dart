import 'package:fitness_app/view/widgets/item_exercise.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExercisesList extends StatelessWidget {
  final List<ExerciseItem> _items;
  ExercisesList({List<ExerciseItem> items}) : _items = items;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: 20),
      scrollDirection: Axis.horizontal,
      separatorBuilder: (context, index) => Container(
        width: 25,
      ),
      itemCount: _items.length,
      itemBuilder: (context, index) {
        return _items[index];
      },
    );
  }
}
