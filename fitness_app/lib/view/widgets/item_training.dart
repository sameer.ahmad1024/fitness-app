import 'package:fitness_app/model/exercise.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TrainingItem extends StatefulWidget {
  final Exercise _exercise;

  TrainingItem({Exercise exercise})
      : _exercise = exercise,
        super();
  @override
  TrainingItemState createState() => TrainingItemState();
}

class TrainingItemState extends State<TrainingItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text("Legs",
                  style: TextStyle(
                      fontFamily: "ABeeZee",
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 30)),
              Text("10 Trainings",
                  style: TextStyle(
                      fontFamily: "ABeeZee",
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 30)),
            ],
          )
        ],
      ),
    );
  }
}
