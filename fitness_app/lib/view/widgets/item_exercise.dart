import 'package:flutter/material.dart';

class ExerciseItem extends StatelessWidget {
  final String _image;
  final String _title;

  ExerciseItem({String image, String title})
      : _image = image,
        _title = title;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(spreadRadius: 0, blurRadius: 4)],
                  image: DecorationImage(
                      image: AssetImage(_image), fit: BoxFit.cover),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                )),
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    stops: [
                      0.0,
                      0.5,
                      1.0
                    ],
                    colors: [
                      Color.fromARGB(0, 15, 50, 74),
                      Color.fromARGB(100, 15, 50, 74),
                      Color.fromARGB(200, 15, 50, 74),
                    ],
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter),
              ),
            )
          ],
        ),
        Text(_title,
            style: TextStyle(
                fontFamily: "ABeeZee",
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18))
      ],
    );
  }
}
