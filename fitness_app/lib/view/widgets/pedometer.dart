import 'package:flutter/material.dart';

class Pedometer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
          Text(
            "PEDOMETER",
            style: TextStyle(
              color: Colors.white,
              fontFamily: "ABeeZee",
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "130",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Orbitron",
                    fontSize: 20,
                  ),
                ),
                Text(
                  "1200 M",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Orbitron",
                    fontSize: 20,
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 15),
            height: 15,
            child: LinearProgressIndicator(
              value: 0.13,
              backgroundColor: Colors.white,
              valueColor: AlwaysStoppedAnimation<Color>(
                Colors.amber,
              ),
            ),
          )
        ]));
  }
}
