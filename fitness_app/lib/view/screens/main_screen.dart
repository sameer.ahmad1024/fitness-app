import 'dart:developer';

import 'package:fitness_app/common/settings.dart';
import 'package:fitness_app/repository/main_repository.dart';
import 'package:fitness_app/service/wger_service.dart';
import 'package:fitness_app/view/widgets/calories_calculator.dart';
import 'package:fitness_app/view/widgets/categories.dart';
import 'package:fitness_app/view/widgets/full_blurred_background.dart';
import 'package:fitness_app/view/widgets/pedometer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainRepository().getExercises(null);
    WorkoutManagerService().get(ENDPOINTS.exercise).then((response) {
      print(response.body.toString());
    }).catchError((onError) {
      log(onError.toString());
    });
    final double categoriesHeight =
        (MediaQuery.of(context).size.width - 40) / 3;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          FullBlurredBackground(
            image: Settings.IMAGE_MAIN_BACKGROUND,
          ),
          Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      stops: [
                0.0,
                1.0
              ],
                      colors: [
                Color.fromARGB(250, 15, 50, 74),
                Color.fromARGB(125, 15, 50, 74)
              ],
                      begin: FractionalOffset.bottomCenter,
                      end: FractionalOffset.topCenter))),
          Container(
            alignment: Alignment.center,
            child: Padding(
              padding:
                  EdgeInsets.only(top: 40, bottom: 20, left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("WORKOUT",
                      style: TextStyle(
                          fontFamily: "ABeeZee",
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 30)),
                  CaloriesCalculator(),
                  Container(
                    height: categoriesHeight + 50,
                    child: Categories(),
                  ),
                  Pedometer()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
