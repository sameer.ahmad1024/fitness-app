import 'package:fitness_app/common/settings.dart';
import 'package:fitness_app/model/exercise.dart';
import 'package:fitness_app/view/widgets/full_blurred_background.dart';
import 'package:fitness_app/view/widgets/item_exercise.dart';
import 'package:fitness_app/view/widgets/list_exercises.dart';
import 'package:flutter/material.dart';

class Exercises extends StatelessWidget {
  final List<ExerciseItem> _items = new List();
  final List<Exercise> _exercises;
  Exercises({List<Exercise> exercises}) : _exercises = exercises {
    _items.addAll([
      ExerciseItem(
        image: "assets/images/legs.jpg",
        title: "Legs",
      ),
      ExerciseItem(
        image: "assets/images/arms.jpg",
        title: "Arms",
      ),
      ExerciseItem(
        image: "assets/images/chest.jpg",
        title: "Chest",
      ),
      ExerciseItem(
        image: "assets/images/abs.jpg",
        title: "Abs",
      ),
      ExerciseItem(
        image: "assets/images/back.jpg",
        title: "Back",
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        FullBlurredBackground(
          image: Settings.IMAGE_MAIN_BACKGROUND,
        ),
        Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    stops: [
              0.0,
              1.0
            ],
                    colors: [
              Color.fromARGB(250, 15, 50, 74),
              Color.fromARGB(125, 15, 50, 74)
            ],
                    begin: FractionalOffset.bottomCenter,
                    end: FractionalOffset.topCenter))),
        Container(
          height: MediaQuery.of(context).size.height / 5 * 2,
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.only(top: 40, bottom: 20, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Excercises",
                    style: TextStyle(
                        fontFamily: "ABeeZee",
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30)),
                ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: 140, minHeight: 100),
                  child: ExercisesList(
                    items: _items,
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
            bottom: 0,
            height: MediaQuery.of(context).size.height / 5 * 3,
            width: MediaQuery.of(context).size.width,
            child: Container(
              padding: EdgeInsets.only(top: 30, left: 10, right: 10),
              decoration: BoxDecoration(
                  color: Color.fromARGB(255, 15, 50, 100),
                  borderRadius:
                      BorderRadius.only(topLeft: Radius.circular(50))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text("Legs",
                          style: TextStyle(
                              fontFamily: "ABeeZee",
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                      Text("10 Exercises",
                          style: TextStyle(
                              fontFamily: "ABeeZee",
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                    ],
                  ),
                ],
              ),
            ))
      ],
    );
  }
}
