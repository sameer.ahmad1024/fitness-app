import 'package:fitness_app/model/equipment.dart';
import 'package:fitness_app/model/muscle.dart';
import 'package:fitness_app/model/exercise_category.dart';
import 'package:fitness_app/model/model.dart';

class ExerciseInfo extends Model {
  String _name;
  ExerciseCategory _category;
  String _description;
  List<Muscle> _muscles;
  List<Muscle> _musclesSecondary;
  List<Equipment> _equipment;

  ExerciseInfo(
      {String name,
      ExerciseCategory category,
      String description,
      List<Muscle> muscles,
      List<Muscle> musclesSecondary,
      List<Equipment> equipment}) {
    this._name = name;
    this._category = category;
    this._description = description;
    this._muscles = muscles;
    this._musclesSecondary = musclesSecondary;
    this._equipment = equipment;
  }

  String get name => _name;
  // set name(String name) => _name = name;
  ExerciseCategory get category => _category;
  // set category(ExerciseCategory category) => _category = category;
  String get description => _description;
  // set description(String description) => _description = description;
  List<Muscle> get muscles => _muscles;
  // set muscles(List<Muscle> muscles) => _muscles = muscles;
  List<Muscle> get musclesSecondary => _musclesSecondary;
  // set musclesSecondary(List<Muscle> musclesSecondary) =>
  // _musclesSecondary = musclesSecondary;
  List<Equipment> get equipment => _equipment;
  // set equipment(List<Equipment> equipment) => _equipment = equipment;

  ExerciseInfo.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _category = json['category'] != null
        ? new ExerciseCategory.fromJson(json['category'])
        : null;
    _description = json['description'];
    if (json['muscles'] != null) {
      _muscles = new List<Muscle>();
      json['muscles'].forEach((v) {
        _muscles.add(new Muscle.fromJson(v));
      });
    }
    if (json['muscles_secondary'] != null) {
      _musclesSecondary = new List<Muscle>();
      json['muscles_secondary'].forEach((v) {
        _musclesSecondary.add(new Muscle.fromJson(v));
      });
    }
    if (json['equipment'] != null) {
      _equipment = new List<Equipment>();
      json['equipment'].forEach((v) {
        _equipment.add(new Equipment.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    if (this._category != null) {
      data['category'] = this._category.toJson();
    }
    data['description'] = this._description;
    if (this._muscles != null) {
      data['muscles'] = this._muscles.map((v) => v.toJson()).toList();
    }
    if (this._musclesSecondary != null) {
      data['muscles_secondary'] =
          this._musclesSecondary.map((v) => v.toJson()).toList();
    }
    if (this._equipment != null) {
      data['equipment'] = this._equipment.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
