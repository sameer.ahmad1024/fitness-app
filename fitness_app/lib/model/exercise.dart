import 'package:fitness_app/model/model.dart';

class Exercise extends Model {
  int _id;
  String _licenseAuthor;
  String _status;
  String _description;
  String _name;
  String _nameOriginal;
  String _creationDate;
  String _uuid;
  int _license;
  int _category;
  int _language;
  List<int> _muscles;
  List<int> _musclesSecondary;
  List<int> _equipments;

  exercise(
      {int id,
      String licenseAuthor,
      String status,
      String description,
      String name,
      String nameOriginal,
      String creationDate,
      String uuid,
      int license,
      int category,
      int language,
      List<int> muscles,
      List<int> musclesSecondary,
      List<int> equipments}) {
    this._id = id;
    this._licenseAuthor = licenseAuthor;
    this._status = status;
    this._description = description;
    this._name = name;
    this._nameOriginal = nameOriginal;
    this._creationDate = creationDate;
    this._uuid = uuid;
    this._license = license;
    this._category = category;
    this._language = language;
    this._muscles = muscles;
    this._musclesSecondary = musclesSecondary;
    this._equipments = equipments;
  }

  int get id => _id;
  // set id(int id) => _id = id;
  String get licenseAuthor => _licenseAuthor;
  // set licenseAuthor(String licenseAuthor) => _licenseAuthor = licenseAuthor;
  String get status => _status;
  // set status(String status) => _status = status;
  String get description => _description;
  // set description(String description) => _description = description;
  String get name => _name;
  // set name(String name) => _name = name;
  String get nameOriginal => _nameOriginal;
  // set nameOriginal(String nameOriginal) => _nameOriginal = nameOriginal;
  String get creationDate => _creationDate;
  // set creationDate(String creationDate) => _creationDate = creationDate;
  String get uuid => _uuid;
  // set uuid(String uuid) => _uuid = uuid;
  int get license => _license;
  // set license(int license) => _license = license;
  int get category => _category;
  // set category(int category) => _category = category;
  int get language => _language;
  // set language(int language) => _language = language;
  List<int> get muscles => _muscles;
  // set muscles(List<int> muscles) => _muscles = muscles;
  List<int> get musclesSecondary => _musclesSecondary;
  // set musclesSecondary(List<int> musclesSecondary) =>
  // _musclesSecondary = musclesSecondary;
  List<int> get equipments => _equipments;
  // set equipment(List<int> equipments) => _equipments = equipments;

  Exercise.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _licenseAuthor = json['license_author'];
    _status = json['status'];
    _description = json['description'];
    _name = json['name'];
    _nameOriginal = json['name_original'];
    _creationDate = json['creation_date'];
    _uuid = json['uuid'];
    _license = json['license'];
    _category = json['category'];
    _language = json['language'];
    if (json['muscles'] != null) {
      _muscles = new List<int>();
      json['muscles'].forEach((v) {
        _muscles.add(v);
      });
    }
    if (json['muscles_secondary'] != null) {
      _musclesSecondary = new List<int>();
      json['muscles_secondary'].forEach((v) {
        _musclesSecondary.add(v);
      });
    }
    if (json['equipment'] != null) {
      _equipments = new List<int>();
      json['equipment'].forEach((v) {
        _equipments.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['license_author'] = this._licenseAuthor;
    data['status'] = this._status;
    data['description'] = this._description;
    data['name'] = this._name;
    data['name_original'] = this._nameOriginal;
    data['creation_date'] = this._creationDate;
    data['uuid'] = this._uuid;
    data['license'] = this._license;
    data['category'] = this._category;
    data['language'] = this._language;
    if (this._muscles != null) {
      data['muscles'] = this._muscles.toList();
    }
    if (this._musclesSecondary != null) {
      data['muscles_secondary'] = this._musclesSecondary.toList();
    }
    if (this._equipments != null) {
      data['equipment'] = this._equipments.toList();
    }
    return data;
  }
}
