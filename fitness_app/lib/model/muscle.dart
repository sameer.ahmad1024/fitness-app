import 'package:fitness_app/model/model.dart';

class Muscle extends Model {
  int _id;
  String _name;
  bool _isFront;

  /// Parametrized Constructor.
  Muscle(this._id, this._name, this._isFront);

  /// Factory Constructor creates a [muscle] instance from json map.
  factory Muscle.fromJson(Map<String, dynamic> json) =>
      Muscle(json['id'], json['name'], json['is_front']);

  /// Convert [muscle] object to json map.
  Map<String, dynamic> toJson() =>
      {'id': _id, 'name': _name, 'is_front': _isFront};

  /// Muscle [id].
  int get id => _id;

  /// Muscle [name].
  String get name => _name;

  /// Whether muscle is front or back.
  bool get isFront => _isFront;
}
