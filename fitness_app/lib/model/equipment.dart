import 'package:fitness_app/model/model.dart';

class Equipment extends Model {
  int _id;
  String _name;

  /// Parametrized constructor.
  Equipment(this._name, this._id);

  /// Initialize [Equipment] object from json data.
  Equipment.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _id = json['id'];
  }

  /// Convert [Equipment] object to json map.
  Map<String, dynamic> toJson() => {'id': _id, 'name': _name};

  /// Equipment [id].
  int get id => _id;

  /// Equipment [name].
  String get name => _name;
}
