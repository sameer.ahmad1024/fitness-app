import 'package:fitness_app/model/model.dart';

class ExerciseCategory extends Model {
  int _id;
  String _name;

  /// Parametrized constructor.
  ExerciseCategory(this._name, this._id);

  /// Initialize [ExerciseCategory] object from json data.
  ExerciseCategory.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _id = json['id'];
  }

  /// Convert [ExerciseCategory] object to json map.
  Map<String, dynamic> toJson() => {'id': _id, 'name': _name};

  /// ExerciseCategory [id].
  int get id => _id;

  /// ExerciseCategory [name].
  String get name => _name;
}
