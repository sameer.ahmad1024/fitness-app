import 'dart:convert';

import 'package:fitness_app/common/settings.dart';
import 'package:http/http.dart' as http;

import 'package:fitness_app/model/exercise_info.dart';
import 'package:fitness_app/service/wger_service.dart';

class MainRepository {
  /// Private constructor.
  MainRepository._internal();

  /// Static instance for singleton design.
  static MainRepository get _instance => MainRepository._internal();

  /// Factory constructor for Singleton design.
  factory MainRepository() => _instance;

  List<ExerciseInfo> _exercises;

  List<ExerciseInfo> get exercises => _exercises ?? getExercises(null);

  /// Get exercises from different sources.
  ///
  /// If the response is OK from the web server,
  /// Get the new data. Otherwise, return current version of it.
  Future<List<ExerciseInfo>> getExercises(Map<String, dynamic> options) async {
    http.Response response = await WorkoutManagerService().get(
        ENDPOINTS.exerciseinfo,
        headers: {'Accept': 'application/json'},
        options: {'name': 'Biceps+Curl+With+Cable'});

    if (response.statusCode == 200) {
      var responseMap = jsonDecode(response.body.toString()) as Map;
      List<ExerciseInfo> exercise = new List();
      for (var json in responseMap['results']) {
        exercise.add(ExerciseInfo.fromJson(json));
      }
      _exercises = exercise;
    }
    return _exercises;
  }
}
